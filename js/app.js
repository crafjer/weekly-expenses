//Global Variables
const form = document.querySelector('#agregar-gasto');

//Clases
class UI {

    showBudget(budgetManager) {
        document.querySelector('#total').textContent = budgetManager.budget;
        document.querySelector('#restante').textContent = budgetManager.remaining;
    }

    showMessage(msg, type) {
    
        const div = document.createElement('div');
        div.classList.add('alert', 'text-center');

        if(type === 'danger') {
            div.classList.add('alert-danger');
        } 
        else if (type === 'success') {
            div.classList.add('alert-success');
        }

        div.textContent = msg;
        console.log(div);

        const first = document.querySelector('.primario');
        first.insertBefore(div, form);

        setTimeout(function(){
            div.remove();
        }, 3000);
    }

    showExpenses(budgetManager) {
        let ul = document.querySelector('#gastos .list-group');
        
        this.cleanHtml(ul);

        budgetManager.expenses.forEach( expenses => {
            const li = document.createElement('li');
            li.className = 'list-group-item d-flex justify-content-between align-items-center';
            li.dataset.id = expenses.id;

            li.innerHTML = `${expenses.name}<span class="budge badge-primary badge-pill">$ ${expenses.amount}</span>`;

            const btnDelete = document.createElement('button');
            btnDelete.innerHTML = 'Borrar &times;';
            btnDelete.className = 'btn btn-danger delete-expenses';
            btnDelete.onclick = () => {
                deleteExpense(expenses.id)
            }

            li.appendChild(btnDelete);

            ul.appendChild(li);

        });
    }

    updateReamining(budgetManager) {
        const {remaining, budget} = budgetManager;
        const divRemaining = document.querySelector('#restante');

        divRemaining.textContent = remaining;
        divRemaining.classList.add('restante');
        
        //test 25% 
        if( (budget / 4) >= remaining ) {
            divRemaining.parentElement.parentElement.className = 'alert alert-danger';
        }
        else if( (budget / 2) >= remaining ) {
            divRemaining.parentElement.parentElement.className = 'alert alert-warning';
        }
        else {
            divRemaining.parentElement.parentElement.className = 'alert alert-success';
        }

        if(remaining <= 0) {
            this.showMessage('El presupuesto se ha agotado.', 'danger');
            form.querySelector('button[type="submit"]').disabled = true;
        }
        
    }

    cleanHtml(el) {
        while( el.firstChild ) {
            el.removeChild(el.firstChild);
        }
    }
}

class BudgetManager {
    constructor(budget) {
        this.budget = budget;
        this.remaining = budget;
        this.expenses = [];
    }

    setExpenses(expenses) {
        this.expenses = [...this.expenses, expenses];
        this.calcalateRemaining();
    }

    calcalateRemaining() {
        let expensed = this.expenses.reduce((total, expense) => total + expense.amount, 0);
        this.remaining = this.budget - expensed;
    }

    deleteBudget(id) {
        this.expenses = this.expenses.filter( (expense) => expense.id !== id);
        this.calcalateRemaining();
    }
}

ui = new UI();
let budgetManager;


//EventListener
eventListeners();
function eventListeners() {
    addEventListener('DOMContentLoaded', getBudget);
    form.addEventListener('submit', addExpenses);
}

function getBudget() {
    let budget = prompt('¿Cúal es tu presupuesto?');

    if(isEmpty(budget) || isNull(budget) || isNaN(budget) || isLess(budget) ){
        window.location.reload();
    }

    budget = Number(budget);

    budgetManager = new BudgetManager(budget);
    
    ui.showBudget(budgetManager);
}

function addExpenses(e) {
    e.preventDefault();
    
    let name = document.querySelector('#gasto').value;
    if(isEmpty(name)) {
        ui.showMessage('El gasto es obligatorio', 'danger');
        return;
    }

    let amount = document.querySelector('#cantidad').value;
    if(isEmpty(amount)) {
        ui.showMessage('La cantidad es obligatoria', 'danger');
        return;
    }
    if(isLess(amount)) {
        ui.showMessage('La cantidad no puede ser menor or igual a zero', 'danger');
    }
    if(isNaN(amount)) {
        ui.showMessage('La cantidad debe ser un número', 'danger');
    }

    amount = Number(amount);

    let expenses = {name, amount, id: Date.now()}
    budgetManager.setExpenses(expenses);
    form.reset();
    ui.showMessage('Gasto agregando correctamente', 'success');
    ui.showExpenses(budgetManager);
    ui.updateReamining(budgetManager);
    
}

function isEmpty(value) {
    return value === '';
}
function isNull(value) {
    return value === null;
}
function isLess(value) {
    return value <= 0;
}
function deleteExpense(id) {
    budgetManager.deleteBudget(id);
    ui.showExpenses(budgetManager);
    ui.updateReamining(budgetManager);
}



